StoreNumbers = input("Input store numbers separated by a single space > ")
StoreList = StoreNumbers.split(" ")
StoreList = list(map(int, StoreList))
RouteList = []
for x in StoreList:
    if x <= 254:
        RouteList.append("route delete -p 10.11."+str(x)+".0 mask 255.255.255.0 192.168.3.80 metric 5")
        RouteList.append("route delete -p 10.111."+str(x)+".0 mask 255.255.255.0 192.168.3.80 metric 5")
        RouteList.append("route delete -p 10.211."+str(x)+".0 mask 255.255.255.0 192.168.3.80 metric 5")
    elif x >= 255 and x <= 508:
        x = x - 254
        RouteList.append("route delete -p 10.12."+str(x)+".0 mask 255.255.255.0 192.168.3.80 metric 5")
        RouteList.append("route delete -p 10.112."+str(x)+".0 mask 255.255.255.0 192.168.3.80 metric 5")
        RouteList.append("route delete -p 10.212."+str(x)+".0 mask 255.255.255.0 192.168.3.80 metric 5")
    elif x >= 509 and x <= 762:
        x = x - 508
        RouteList.append("route delete -p 10.13."+str(x)+".0 mask 255.255.255.0 192.168.3.80 metric 5")
        RouteList.append("route delete -p 10.113."+str(x)+".0 mask 255.255.255.0 192.168.3.80 metric 5")
        RouteList.append("route delete -p 10.213."+str(x)+".0 mask 255.255.255.0 192.168.3.80 metric 5")

with open('bye_bye_routes.bat', 'w+') as f:
    for item in RouteList:
        f.write("%s\n" % item)